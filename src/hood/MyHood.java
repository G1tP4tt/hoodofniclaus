package hood;

import javax.swing.SwingUtilities;
import javax.swing.Timer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class MyHood {

	private static final int DELAY = 500;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}

	private static void createAndShowGUI() {
		System.out.println("Created GUI on EDT? " + SwingUtilities.isEventDispatchThread());
		JFrame f = new JFrame("Niclaus' Hood");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(500, 500);
		MyPanel panel = new MyPanel();
		f.add(panel);

		new Timer(DELAY, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				panel.paintComponent(panel.getGraphics());
			}

		}).start();

		f.setVisible(true);
	}

}

class MyPanel extends JPanel {

	int x1, y1, x2, y2;
	int n = 1;

	public MyPanel() {
		setBorder(BorderFactory.createLineBorder(Color.black));
	}

	
//

	public void paintComponent(Graphics g) {

		if (n > 8)
			super.paintComponent(g);

		update();
		
		g.drawLine(x1, y1, x2, y2);

	}

	private void update() {

		switch (n) {
		case 1:
			x1 = 50;
			y1 = 450;
			x2 = 450;
			y2 = 450;
			break;
		case 2:
			x1 = 450;
			y1 = 450;
			x2 = 50;
			y2 = 200;
			break;
		case 3:
			x1 = 50;
			y1 = 200;
			x2 = 450;
			y2 = 200;
			break;
		case 4:
			x1 = 450;
			y1 = 200;
			x2 = 50;
			y2 = 450;
			break;
		case 5:
			x1 = 50;
			y1 = 450;
			x2 = 50;
			y2 = 200;
			break;
		case 6:
			x1 = 50;
			y1 = 200;
			x2 = 250;
			y2 = 0;
			break;
		case 7:
			x1 = 250;
			y1 = 0;
			x2 = 450;
			y2 = 200;
			break;
		case 8:
			x1 = 450;
			y1 = 200;
			x2 = 450;
			y2 = 450;
			break;
		default:
			x1 = 50;
			y1 = 450;
			x2 = 450;
			y2 = 450;
			n = 1;
			break;

		}
		n++;
	}

}

class Line {
	int x1, y1, x2, y2;

	Line(int x1, int y1, int x2, int y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2;
		this.y2 = y2;
	}
}
